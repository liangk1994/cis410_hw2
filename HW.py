import math
file = open("gameShapley.txt", 'r')

raw_input = []
for line in file:
	raw_input.append(line.split())

file.close

full_list = []
size = 0
for i in range(len(raw_input)):
	if(i == 0):
		size = int(raw_input[i][0])
		continue
	else:
		for x in raw_input[i]:
			temp_item = x.split("}")
			temp_item[0]= temp_item[0].replace("{", "")
			temp_item[0]= temp_item[0].split(",")
			temp_item[1]= temp_item[1].replace(",", "")
			full_list.append(temp_item)

output = []
file = open("Shapley.txt", 'w')
for i in range(size):
	output_int = 0
	for j in range(len(full_list)):
		if str(i+1) in full_list[j][0]:
			continue
		else:
			temp_id = full_list[j]
			temp_union_id = None
			for x in range(len(full_list)):
				# print(str(full_list[x][0]))
				if (str(i+1) in full_list[x][0]) and (set(temp_id[0]).issubset(full_list[x][0])):
					temp_union_id = full_list[x]
					break
			print("C: " + str(len(temp_id[0])) + ", N: " + str(size) + ", v of union: " + str(temp_union_id[1]) + ", v of C: " + str(temp_id[1]))
			output_int += math.factorial(len(temp_id[0]))* math.factorial(size - len(temp_id[0]) - 1) * (int(temp_union_id[1]) - int(temp_id[1]))
	output_int += math.factorial(0) * math.factorial(size - 1) * int(full_list[i][1])
	output_int = output_int * (1/math.factorial(size))
	print(output_int)
	file.write("player " + str(i+1) + ": " + str(output_int) + "\n")
file.close